# Desmos Enhancements
An extension to improve the experience on the Desmos graphing calculator.

## Current Features
+ Right click for a custom colour

## WIP Features
+ Import/Export JSON

## Planned features
+ Dark mode
