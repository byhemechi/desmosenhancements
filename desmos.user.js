// ==UserScript==
// @name         Desmos Enhancements
// @version      1
// @description  Various improvements for the desmos graphing calculator
// @author       George Fischer (byhemechi)
// @match        https://*.desmos.com/calculator*
// @grant        none
// @namespace    https://byhe.me/chi
// ==/UserScript==

(function() {
    'use strict';

    const extstate = Math.random().toString(36).replace(/^0\./, "");
console.info(`Any elements created by the DES extension will have the class .%cdes-${extstate}`, "font-family: monospace, color: #00aac4");

const inputStyle = {
    width: "30px",
    height: "30px",
    padding: 0,
    border: 0,
    background: "transparent",
    borderRadius: "50%",
    position: "absolute",
    bottom: 0,
    right: 0,
    opacity: 0,
    display: "none"
}

function update() {
    // Delete all pre-existing elements
    document.querySelectorAll(`.des-${extstate}`).forEach(function(i) {
        i.parentElement.removeChild(i);
    })

    const graphState = Calc.getState();

    document.querySelectorAll(".dcg-expression-icon-container").forEach(function(i, n) {
        const expression = graphState.expressions.list[n];
        const colourbox = document.createElement("input");
        Object.assign(colourbox.style, inputStyle)
        colourbox.value = expression.color;
        i.oncontextmenu = function(e) {
            e.preventDefault()
            colourbox.value = Calc.getExpressions()[n].color
            colourbox.click()
        }
        colourbox.classList.add("des-" + extstate)
        colourbox.onchange = function() {
            const ce = Calc.getExpressions()[n];
            ce.color = colourbox.value;
            Calc.setExpression(ce)
        }

        colourbox.type='color'
        i.appendChild(colourbox)
    });
}

update()


const observer = new MutationObserver(function(...e) {
    update()
});
observer.observe(document.querySelector(".dcg-template-expressioneach"), { childList: true })
})();
